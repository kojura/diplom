module weather-Norway

go 1.15

require (
	github.com/jackc/pgproto3/v2 v2.1.0 // indirect
	github.com/labstack/echo v3.3.10+incompatible // indirect
	github.com/labstack/echo/v4 v4.3.0
	github.com/mattn/go-isatty v0.0.13 // indirect
	golang.org/x/crypto v0.0.0-20210616213533-5ff15b29337e // indirect
	golang.org/x/net v0.0.0-20210614182718-04defd469f4e // indirect
	golang.org/x/sys v0.0.0-20210616094352-59db8d763f22 // indirect
	golang.org/x/time v0.0.0-20210611083556-38a9dc6acbc6 // indirect
	gorm.io/driver/postgres v1.1.0
	gorm.io/gorm v1.21.11
)
